/**
 * Конвертитрует данные пришедшие из neo4j с флагом
 * resultDataContents: ["row","graph"]
 * @param data
 * @returns {{table, graph: {nodes: Array, links: Array}, labels: Array}}
 */

export function graphDataConverter (data) {
    if(!data.errors && !data.results) return;
    if (data.errors.length > 0) throw new Error("Cannot parse data: " + JSON.stringify(data.errors));
    // Определить cols и rows таблицы
    let cols = data.results[0].columns;
    let rows = data.results[0].data.map((row) => {
        let r = {};
        cols.forEach((col, index) => {
            r[col] = row.row[index];
        });
        return r;
    });
    // Узлы связи и лейблы neo4j
    let nodes = [];
    let rels = [];
    let labels = [];
    // Идем
    data.results[0].data.forEach(function (row) {
        // Узлы
        row.graph.nodes.forEach(function (node) {
            // Ищеи узел из тех что уже были найдены, чтобы предотвратить дублирование
            // из структуры возвращаемой neo4j
            let found = nodes.filter(function (nodesListItem) {
                    return nodesListItem.id === node.id;
                }).length > 0;
            // Если такогого нет, добавляем узел в наш объект
            if (!found) {
                for (let prop in node.properties || {}) {
                    node[prop] = node.properties[prop];
                    delete node.properties[prop];
                }
                delete node.properties;

                nodes.push(node);
                // Не забываем про лейблы, держим их обновленными. То есть, если на новой итерации
                // мы найдем новый лейбл, он будет добавлен в массив
                labels = labels.concat(node.labels.filter(function (label) {
                    return labels.indexOf(label) === -1;
                }));
            }
        });
        // Собираем коллекцию уникальных линков
        row.graph.relationships.forEach(function(rel) {
            let found = rels.filter(function (storedRel) { return rel.id === storedRel.id }).length > 0;
            if (!found) {
                rels.push(rel);
            }
        });
    });
    //  Связи. Тут все просто. Мапим в вид, принимаемый D3
    rels = rels.map(
        function (rel) {
            return {
                id: rel.id,
                source: rel.startNode,
                target: rel.endNode,
                type: rel.type
            };
        }
    );

    // Проверяем выходит/входит ли из одного нода более одной связи и сохраняем их количество
    rels.forEach(function (link, index, array) {
        let sample = {"target": link.target, "source": link.source};
        link.isMultiples = 0;
        array.forEach(function (item, innerIndex) {
            if (sample.target === item.source && sample.source === item.target) {
                link.isMultiples++;
            }
        });
        if (link.isMultiples === 1) delete link.isMultiples;
    });

    return {table: rows, graph: {nodes: nodes, links: rels}, labels: labels};
}
/**
 * Соеденяет данные графа уже в конвертированном виде
 * @param currentData
 * @param mixin
 */
export function graphDataMerger (currentData, mixin) {
 // Проверка на оригинальность уточненных данных, возможно просто нужно не отдавать другие связи
    // let newNodes = mixin.results[0].data.nodes.filter(function(node) {
    //     return currentData.results[0].data.nodes
    // });
    //
    // let newRels = mixin.results[0].data.relationships.filter(function(rel))


    return {
        errors: currentData.errors.concat(mixin.errors),
        results: [
            {
                columns: currentData.results[0].columns.concat(mixin.results[0].columns),
                data: currentData.results[0].data.concat(mixin.results[0].data)
            }
        ]
    }
}
