const ACCESS_TOKEN = 'access-token'
const REFRESH_TOKEN = 'refresh-token'

export function getToken() {
  return localStorage.getItem(ACCESS_TOKEN)
}

export function setTokens(data) {
  localStorage.setItem(ACCESS_TOKEN, data.access_token)
  localStorage.setItem(REFRESH_TOKEN, data.refresh_token)
}

export function removeTokens() {
  localStorage.removeItem(ACCESS_TOKEN)
  localStorage.removeItem(REFRESH_TOKEN)
}

export function getRefreshToken() {
  return localStorage.getItem(REFRESH_TOKEN)
}
