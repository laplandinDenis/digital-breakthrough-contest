export function generateTitle(title) {
  return this.$t(title) // $t :this method from vue-i18n ,inject in @/lang/index.js
}

export function generateAddTitle(title) {
  return this.$t(`add.${title}`)
}
