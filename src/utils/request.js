import axios from 'axios'
import { Message } from 'element-ui'
import store from '@/store'
import router from '@/router'
import { getToken } from '@/utils/auth'

let requestQueue = []
let lastRequest = {}
// axious instance create
const service = axios.create({
  baseURL: process.env.BASE_API, // api base_url
  timeout: 5000 // Request timeout
})

/**
 * axios decorator for control is token refreshing now and manage queue of requests
 *
 * @param config
 * @return promise
 */

function serviceDecorator(config) {
  let promise
  // Return promises for requests stacks in queue
  if (service.isPending) {
    promise = new Promise(function(resolve, reject) {
      requestQueue.push({ config, resolve })
    })
    return promise
  }
  // Return request for last request for repeat if needed
  return new Promise(async function(resolve, reject) {
    lastRequest = ({ config, resolve })
    try {
      const response = await service(config)
      resolve(response)
    } catch (error) {
      console.log(`Error in request: ${error}`)
    }
  })
}

// request interceptor
service.interceptors.request.use(config => {
  const token = store.getters.token || getToken()
  if (token) {
    // Set auth header before each request
    // config.headers['Authorization'] = `Bearer ${store.getters.token}`
    config.headers['Authorization'] = `Bearer ${token}`
  }
  return config
}, error => {
  // Do something with request error
  console.log(error) // for debug
  Promise.reject(error)
})

// response interceptor
service.interceptors.response.use(
  response => {
    lastRequest = {} // clear last request
    return response
  },
  async error => {
    console.log(`err ${error}`) // for debug

    async function refreshToken() {
      try {
        await store.dispatch('RefreshToken')
      } catch (err) {
        Message.error('Ошибка авторизации, пожалуйста пройдите авторизацию')
        router.push({ path: '/login' })
      }
    }
    function resendPendingRequests() {
      requestQueue.forEach(async deferredRequest => {
        const config = deferredRequest.config
        const resolve = deferredRequest.resolve
        console.log('>>>', config, resolve)
        try {
          const response = await service(config)
          resolve(response)
        } catch (error) {
          console.log(`errResolve ${error}`)
        }
      })
      requestQueue = [] // clear queue
    }

    /**
     * Handling unauthorized error caused by invalid access_token
     */
    if (error.response.status === 401) {
      this.isPending = true
      requestQueue.push(lastRequest)
      await refreshToken()
      this.isPending = false
      resendPendingRequests()
    } else {
      Message({
        message: error.message,
        type: 'error',
        duration: 5 * 1000
      })
    }
    return Promise.reject(error)
  })

export default serviceDecorator
