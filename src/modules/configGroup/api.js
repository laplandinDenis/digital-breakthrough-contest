import request from '@/utils/request'

export function fetchGroupConfigList(params) {
  return request({
    url: '/api/v1/groups/configs.json',
    method: 'get',
    params
  })
}

export function fetchGroupConfigItem(id, params) {
  return request({
    url: `/api/v1/groups/configs/${id}.json`,
    method: 'get',
    params
  })
}

export function postGroupConfig(data) {
  return request({
    url: '/api/v1/groups/configs',
    method: 'post',
    data
  })
}

export function deleteGroupConfig(id) {
  return request({
    url: `/api/v1/groups/configs/${id}`,
    method: 'delete'
  })
}

export function putGroupConfig(id) {
  return request({
    url: `/api/v1/groups/configs/${id}.json`,
    method: 'put'
  })
}

export function fetchConfigList(params) {
  return request({
    url: `/api/v1/configs.json`,
    method: 'get',
    params
  })
}

export function fetchConfig(id, params) {
  return request({
    url: `/api/v1/configs/${id}`,
    method: 'get',
    params
  })
}

export function postConfig(data) {
  return request({
    url: '/api/v1/configs',
    method: 'post',
    data
  })
}

export function putConfig(id, data) {
  return request({
    url: `/api/v1/configs/${id}`,
    method: 'put',
    data
  })
}

export function deleteConfig(id) {
  return request({
    url: `/api/v1/configs/${id}`,
    method: 'delete'
  })
}

export function sendConfigsToDevices(data) {
  return request({
    url: `/api/v1/sendings/configs`,
    method: 'post',
    data
  })
}
