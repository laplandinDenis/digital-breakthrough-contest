const _import = require('@/utils/import/_import_' + process.env.NODE_ENV)

export default [
  {
    path: '/config-group',
    component: {
      template: '<router-view></router-view>'
    },
    name: 'configGroup',
    meta: {
      title: 'CONFIG_GROUP.PAGE_TITLE.MENU',
      noDropdown: true,
      icon: 'form'
    },
    children: [
      {
        path: '',
        component: _import('configGroup/containers/list'),
        name: 'configGroup.list'
      },
      {
        path: 'info/:id',
        component: _import('configGroup/containers/info'),
        name: 'configGroup.info',
        hidden: true,
        meta: { title: 'CONFIG_GROUP.PAGE_TITLE.VIEW' }
      },
      {
        path: 'edit/:id',
        component: {
          template: '<router-view></router-view>'
        },
        name: 'configGroup.edit',
        redirect: { name: 'configGroup.edit.list' },
        hidden: true,
        meta: { title: 'CONFIG_GROUP.PAGE_TITLE.EDIT' },
        children: [
          {
            path: '',
            component: _import('configGroup/containers/edit'),
            name: 'configGroup.edit.list'
          },
          {
            path: 'create',
            component: _import('configGroup/containers/createConfig'),
            hidden: true,
            name: 'configGroup.edit.create',
            meta: { title: 'CONFIG_GROUP.CREATE.CREATE' }
          }
        ]
      },
      {
        path: 'config/:id/edit',
        name: 'configGroup.editConfig',
        component: _import('configGroup/containers/editConfig'),
        hidden: true
      }
    ]
  }
]
