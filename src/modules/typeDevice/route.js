const _import = require('@/utils/import/_import_' + process.env.NODE_ENV)

export default [
  {
    path: '/device-type',
    component: {
      template: '<router-view></router-view>'
    },
    name: 'typeDevice',
    meta: {
      title: 'TYPE_DEVICE.PAGE_TITLE.MENU',
      noDropdown: true,
      icon: 'form'
    },
    children: [
      {
        path: '',
        component: _import('typeDevice/containers/list'),
        name: 'typeDevice.list'
      },
      {
        path: 'create',
        component: _import('typeDevice/containers/create'),
        name: 'typeDevice.create',
        hidden: true,
        meta: { title: 'TYPE_DEVICE.PAGE_TITLE.CREATE' }
      },
      {
        path: 'edit/:id',
        component: _import('typeDevice/containers/edit'),
        name: 'typeDevice.edit',
        hidden: true,
        meta: { title: 'TYPE_DEVICE.PAGE_TITLE.EDIT' }
      },
      {
        path: 'info/:id',
        component: _import('typeDevice/containers/info'),
        name: 'typeDevice.info',
        hidden: true,
        meta: { title: 'TYPE_DEVICE.PAGE_TITLE.INFO' }
      }
    ]
  }
]
