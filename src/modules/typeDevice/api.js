import request from '@/utils/request'

export function fetchTypeDeviceList(params) {
  return request({
    url: '/api/v1/type/devices.json',
    method: 'get',
    params: params
  })
}

export function fetchTypeDeviceItem(id, params) {
  return request({
    url: `/api/v1/type/devices/${id}.json`,
    method: 'get',
    params: params
  })
}

export function postTypeDevice(data) {
  return request({
    url: '/api/v1/type/devices',
    method: 'post',
    data
  })
}

export function deleteTypeDevice(id) {
  return request({
    url: `/api/v1/type/devices/${id}.json`,
    method: 'delete'
  })
}

export function updateTypeDevice(id, data) {
  return request({
    url: `/api/v1/type/devices/${id}.json`,
    method: 'put',
    data
  })
}
