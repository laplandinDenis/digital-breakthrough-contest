import request from '@/utils/request'

export function fetchQueueList(params) {
  return request({
    url: '/api/v1/queues.json',
    method: 'get',
    params: params
  })
}

export function fetchQueueItem(id) {
  return request({
    url: `/api/v1/queues/${id}.json`,
    method: 'get'
  })
}

export function deleteQueueItem(id) {
  return request({
    url: `/api/v1/queues/${id}.json`,
    method: 'delete'
  })
}
