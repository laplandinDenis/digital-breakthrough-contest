const _import = require('@/utils/import/_import_' + process.env.NODE_ENV)

export default [
  {
    path: '/queue',
    component: {
      template: '<router-view></router-view>'
    },
    name: 'queue',
    meta: {
      title: 'QUEUE.PAGE_TITLE.MENU',
      noDropdown: true,
      icon: 'form'
    },
    children: [
      {
        path: '',
        component: _import('queue/containers/list'),
        name: 'queue.list'
      },
      {
        path: ':id',
        component: _import('queue/containers/info'),
        name: 'queue.info'
      }
    ]
  }
]
