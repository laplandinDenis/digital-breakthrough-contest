export default {
  QUEUE: {
    PAGE_TITLE: {
      MENU: 'Очередь отправки',
      LIST: 'Очередь отправки',
      INFO: 'Просмотр действия',
      EDIT: 'Редактирование очереди'
    }
  }
}
