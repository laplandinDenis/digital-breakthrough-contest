const _import = require('@/utils/import/_import_' + process.env.NODE_ENV)

export default [
  {
    path: '/device',
    component: {
      template: '<router-view></router-view>'
    },
    name: 'device',
    meta: {
      icon: 'form',
      title: 'DEVICE.PAGE_TITLE.MENU',
      noDropdown: true
    },
    children: [
      {
        path: '',
        component: _import('device/containers/list'),
        name: 'device.list'
      },
      {
        path: 'create',
        component: _import('device/containers/create'),
        name: 'device.create',
        hidden: true,
        meta: {
          title: 'DEVICE.PAGE_TITLE.CREATE'
        }
      },
      {
        path: 'edit/:id',
        component: _import('device/containers/edit'),
        name: 'device.edit',
        hidden: true,
        meta: {
          title: 'DEVICE.PAGE_TITLE.EDIT'
        }
      },
      {
        path: 'info/:id',
        component: _import('device/containers/info'),
        name: 'device.info',
        hidden: true,
        meta: {
          title: 'DEVICE.PAGE_TITLE.EDIT'
        }
      }
    ]
  }
]
