import { fetchDeviceList, fetchDevice } from './api'

const devices = {
  state: {
    list: [],
    current: {}
  },
  mutations: {
    SET_DEVICES_LIST: (state, list) => {
      state.list = list
    },
    SET_DEVICES_CURRENT: (state, item) => {
      state.current = item
    }
  },
  actions: {
    async getDevicesList({ commit }) {
      const response = await fetchDeviceList()
      commit('SET_DEVICES_LIST', response.data.items)
    },
    async getDevicesItem({ commit }, id) {
      const response = await fetchDevice(id)
      commit('SET_DEVICES_CURRENT', response.data)
    }
  }
}

const devicesGetters = {
  devicesList: state => state.devices.list,
  currentDevice: state => state.devices.current
}

export { devices, devicesGetters }
