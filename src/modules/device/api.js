import request from '@/utils/request'

/**
 *
 * @param params
 * available params list: limit, start, orderby, orderbydesc
 */
export function fetchDeviceList(params) {
  return request({
    url: '/api/v1/devices.json',
    method: 'get',
    params
  })
}

export function fetchDevice(id) {
  return request({
    url: `/api/v1/devices/${id}.json`,
    method: 'get'
  })
}

export function postDevice(data) {
  return request({
    url: `/api/v1/devices`,
    method: 'post',
    data
  })
}

export function deleteDevice(id) {
  return request({
    url: `/api/v1/devices/${id}.json`,
    method: 'delete'
  })
}

export function updateDevice(id, data) {
  return request({
    url: `/api/v1/devices/${id}.json`,
    method: 'put',
    data
  })
}
