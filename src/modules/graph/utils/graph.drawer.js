// import * as d3 from "d3";
import d3 from "../utils/context-menu.js";
import "d3-selection-multi";
import dictionary from "../lang/dictionary.json";
import menuData from  "../meta/context-menu-content.json";
import Stack from "@/utils/StackClass";
import { EventBus } from '@/utils/eventBus';
import nodeTypes from "../meta/nodeTypes.json";

let colors = d3.schemeCategory20c;
colors.shift();
colors.shift();

const defaultOptions = {
  w: 1200,
  h: 500,
  calcHeight () {
    return window.innerHeight - 350;
  },
  r: 25,
  colors: colors,
  nodeWordLimit: 3,
  textPadding: 4,
  textSide () {
    return 2 * this.r * Math.cos(Math.PI / 4) - this.textPadding;
  },
  textSideOffset () {
    return this.r - this.textSide() / 2;
  }
};

const defaultData = {
  graph: {nodes:[],links:[]}
};

/**
 * Считаем длину релейшенов
 * @param d
 * @returns {string}
 * @private
 */

var _relsCalc = function (d) {
  const diffX = d.target.x - d.source.x;
  const diffY = d.target.y - d.source.y;

  // Длина пути между центрами нод
  const pathLength = Math.sqrt((diffX * diffX) + (diffY * diffY));

  // x и y - расстояния от центра внешнего края до ноды-цели
  const offsetX = (diffX * this.opts.r) / pathLength;
  const offsetY = (diffY * this.opts.r) / pathLength;

  if (d.isMultiples) {
    let dr = Math.sqrt(diffX * diffX + diffY * diffY);
    return "M " +
      d.source.x + "," +
      d.source.y + "A" +
      dr + "," + dr + " 0 0,1 " +
      (d.target.x - offsetX) + "," +
      (d.target.y - offsetY);
  }
  return "M" + d.source.x + "," + d.source.y + "L" + (d.target.x - offsetX) + "," + (d.target.y - offsetY);
};

/**
 * Основной класс
 */
class SciGraph {
  constructor(mountNode, opts, data=defaultData) {
    this.opts = Object.assign(opts,defaultOptions);
    this.data = data;
    this.mountNode = mountNode;
    this.drawCanvas();
    this.update(data);
    // Карринг
    _relsCalc = _relsCalc.bind(this);
  }

  /**
   * Рисуем главный холст и доп плюшки
   */

  drawCanvas () {
    // Добавляем SVG холст в узел для отображения
    d3.select(this.mountNode)
      .append("svg")
      .attr("preserveAspectRatio", "xMidYMid meet")
      .attr("viewBox", "0 0 " + this.opts.w + " " + this.opts.h)
      .attr("id", "graph")
      // .attr("width", this.opts.w)
      // .attr("height", this.opts.h)
      // .attr("height", this.opts.h())
      .on("click", this.handleCanvasClick.bind(this));
    // Базовые определения
    this.svg = d3.select("#graph");
    this.ctxMenuCache = new Stack();
    // Прототипируем
    Object.assign(this,{link:null,rels:[],relsLabels:[],node:null,ctxMenuElement:null,ctxMenuContent:null});
    // Глобальный SVG холст
    this.svg.append("defs")
      .append("marker")
      .attrs({"id": "arrowhead",
        "viewBox": "-0 -5 10 10",
        "refX": 6,
        "refY": 0,
        "orient": "auto",
        "markerWidth": 10,
        "markerHeight": 10,
        "xoverflow": "visible"})
      .append("svg:path")
      .attr("d", "M 0,-3 L 6 ,0 L 0,3")
      .attr("fill", "#999")
      .style("stroke", "none");
    // Глобальная группа обертка
    this.g = this.svg.append("g")
      .attr("class", "everything");
    EventBus.$emit("svg-ready"); // Уведомляем о создании svg
    // Зум и перетягивание
    this.svg.call(d3.zoom()
      .on("zoom", this.zoommed.bind(this))
    );

    this.g.call(d3.drag()
      .on("start", this.dragstarted.bind(this))
      .on("drag", this.dragged.bind(this))
    );
    console.log('SVG', this.svg)
  }

  /**
   * Хэндлер глобального клика
   * @param e
   */
  handleCanvasClick(e){
    // Денис, обрати внимание, что каждый эвент внутри нашего канваса должен дублироваться в глобальный EventBus
    EventBus.$emit('canvas-click', e);
    this.clearSelection();
  }

  /**
   * Обновить данные графа
   * @param links
   * @param nodes
   */
  update (data) {

    const links = data.graph.links;
    const nodes = data.graph.nodes;
    const labels = data.labels.slice();

    this.clear();

    this.simulation = d3.forceSimulation()
      .force("link", d3.forceLink().id(function (d) {return d.id;}).distance(220).strength(1))
      .force("charge", d3.forceManyBody().strength(0))
      // .force("center", d3.forceCenter(this.opts.w / 2, this.opts.h / 2))
      .force("center", d3.forceCenter(this.opts.w / 2, this.opts.calcHeight() / 2))
      .force("collision", d3.forceCollide().radius(65));

    this.simulation
      .nodes(nodes)
      .on("tick", this.ticked.bind(this));

    this.simulation.force("link")
      .links(links);

    this.link = this.g.selectAll(".link")
      .data(links)
      .enter()
      .append("g")
      .attr("class", "link");

    this.relsOverlay = this.link.append("path")
      .attr("class", "rels-overlay");

    this.rels = this.link.append("path")
      .attrs({
        "class": "relationships",
        "marker-end": "url(#arrowhead)",
        "id": function (d, i) {return "linkID" + i}
      });

    this.relsLabels = this.link.append("text")
      .style("text-anchor", "middle")
      .style("pointer-events", "none")
      .style("font-size", "8px")
      .style("fill", "#000")
      .attrs({
        "class": "rel-label",
        "dx": "12",
        "dy": "-2"
      });

    this.relsLabels.append("textPath")
      .attr("xlink:href", function (d, i) { return "#linkID" + i;})
      .attr("startOffset", "50%")
      .style("text-anchor", "middle")
      .text(function (d) {return dictionary[d.type]});

    // Рендерим узлы
    const _this = this;

    this.node = this.g.selectAll(".node")
      .data(nodes)
      .enter()
      .append("g")
      .attr("class", "node")
      .on("click", function () {
        _this.ctxMenuContent = menuData.main.slice();
        _this.ctxMenuCache.push("main");

        d3.event.stopPropagation();
        _this.clearSelection();

        let self = d3.select(this);
        self.classed("node--selected", !self.classed("node--selected"))
          .classed("node--ctx-menu", true);

        if (!_this.ctxMenuElement) _this.ctxMenuElement = new d3.RadialMenu();

        _this.ctxMenuElement.radius(_this.opts.r + 8)
          .padding(4)
          .thickness(35)
          .animationDuration(250)
          .onClick((d)=> {
            d3.event.stopPropagation();
            const overallData = Object.assign(d,self.data()[0]);
            EventBus.$emit('menu-click',overallData,d3.event);
            //_this.menuUpdate(d.data.action);
          })
          .appendTo(".node--ctx-menu")
          .show(_this.ctxMenuContent);
      })
      .call(d3.drag()
        .on("start", this.dragstarted.bind(this))
        .on("drag", this.dragged.bind(this))
      );

    this.node.append("circle")
      .attr("class", "node--hover")
      .attr("r", this.opts.r + 5)
      .style("fill", "none")
      .style("stroke", "#6ac6ff")
      .style("stroke-width", 5);
    
    this.node.append("circle")
      .attr("r", this.opts.r)
      .style("fill", (d, i) => {
        return this.opts.colors[labels.indexOf(d.labels[0]) * 2 + 1 ];
      })
      .style("stroke-width", "4px")
      .style("stroke", (d, i) => {return this.opts.colors[labels.indexOf(d.labels[0]) * 2];});

    this.node.append("title")
      .text(function (d) {return d.fullName || d.title;});

    this.nodeTextContainer = this.node.append('g')
    // .attr('transform', 'translate(' + [this.opts.textSideOffset(), this.opts.textSideOffset()] + ')');
      .attr('transform', 'translate(' + [-this.opts.r + this.opts.textSideOffset(), -this.opts.r + this.opts.textSideOffset()] + ')');

    this.nodeTextContainer.append("foreignObject")
      .attr("width", this.opts.textSide())
      .attr("height", this.opts.textSide())
      .append("xhtml:body")
      .attr("class", "node-body")
      .style("font", "8px 'Helvetica Neue'")
      .style("line-height", "12px")
      .append("xhtml:p")
      .attr("class", "node-caption")
      .html(function (d) {
        let nodeTitleField = nodeTypes[d.labels[0]][0];
        let nodeText = d[nodeTitleField].trim().split(" ");
        let textLength = nodeText.length;
        nodeText.splice(3);
        if (textLength > 3) return nodeText.join("\n") + "\n...";
        return nodeText.join("\n");
      });
  }

  /**
   * Удалить все объекты и их
   * листенеры
   */
  clear(){
    this.g.selectAll(".link").remove();
    this.g.selectAll(".node").remove();
  }

  /**
   * Анимация, перетягивания, коллизии и остальной стафф
   * хэндлиться тут
   */
  ticked () {

    this.link
      .attr("d", function (d) {
        let dx = d.target.x - d.source.x;
        let dy = d.target.y - d.source.y;
        if (d.isMultiples) {
          let dr = Math.sqrt(dx * dx + dy * dy);
          return "M " +
            d.source.x + "," +
            d.source.y + "A" +
            dr + "," + dr + " 0 0,1 " +
            d.target.x + "," +
            d.target.y;
        }

        return "M " +
          d.source.x + "," +
          d.source.y + "L" +
          d.target.x + "," +
          d.target.y;
      });

    this.node
      .attr("transform", function (d) {return "translate(" + d.x + ", " + d.y + ")";});

    this.rels.attr("d", _relsCalc);
    this.relsOverlay.attr("d", _relsCalc);

    // Трансформация релейшенов в реальном времени
    this.relsLabels.attr("transform", function (d) {
      if (d.target.x < d.source.x) {
        let bbox = this.getBBox();

        let rx = bbox.x + bbox.width / 2;
        let ry = bbox.y + bbox.height / 2;
        return "rotate(180 " + rx + " " + ry + ")";
      } else {
        return "rotate(0)";
      }
    });
  }

  dragstarted (d) {
    if(!d) return;
    if (!d3.event.active) this.simulation.alphaTarget(0.3).restart();
    d.fx = d.x;
    d.fy = d.y;
  }

  dragged (d) {
    if(!d) return;
    d.fx = d3.event.x;
    d.fy = d3.event.y;
  }

  zoommed () {
    this.g.attr("transform", d3.event.transform);
  }

  clearSelection () {
    d3.select(".node--ctx-menu")
      .classed("node--ctx-menu node--selected", false);
    if (this.ctxMenuElement) {
      this.ctxMenuElement.hide();
      this.ctxMenuCache.clear();
    }
  }

  menuUpdate (newContent) {
    this.ctxMenuContent = [];
    this.ctxMenuElement.show(this.ctxMenuContent);
    const newMenuData = menuData[this.ctxMenuCache.peek()];
    if (newContent === "back") {
      this.ctxMenuCache.pop();
      if(newMenuData) this.ctxMenuContent = newMenuData;
    } else {
      this.ctxMenuCache.push(newContent);
      if(newMenuData) this.ctxMenuContent = newMenuData.slice();
    }
    this.ctxMenuElement.show(this.ctxMenuContent);
  }

}

export {colors, SciGraph};
