const _import = require('@/utils/import/_import_' + process.env.NODE_ENV)

export default [
  {
    path: '/graph',
    component: _import('graph/containers/SciGraph/SciGraph'),
    name: 'graph'
  }
]
