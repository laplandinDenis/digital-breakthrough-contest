const _import = require('@/utils/import/_import_' + process.env.NODE_ENV)

export default [
  {
    path: '/device-group',
    component: {
      template: '<router-view></router-view>'
    },
    name: 'device-group',
    meta: {
      title: 'GROUP_DEVICE.PAGE_TITLE.MENU',
      icon: 'table',
      noDropdown: true
    },
    children: [
      {
        path: '',
        component: _import('groupDevice/containers/list'),
        name: 'groupDevice.list'
      },
      {
        path: 'create',
        component: _import('groupDevice/containers/create'),
        name: 'groupDevice.create',
        hidden: true,
        meta: { title: 'GROUP_DEVICE.PAGE_TITLE.CREATE' }
      },
      {
        path: 'edit/:id',
        component: _import('groupDevice/containers/edit'),
        name: 'groupDevice.edit',
        hidden: true,
        meta: { title: 'GROUP_DEVICE.PAGE_TITLE.EDIT' }
      },
      {
        path: 'info/:id',
        component: _import('groupDevice/containers/info'),
        name: 'groupDevice.info',
        hidden: true,
        meta: { title: 'GROUP_DEVICE.PAGE_TITLE.INFO' }
      }
    ]
  }
]
