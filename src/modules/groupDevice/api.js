import request from '@/utils/request'

export function fetchGroupDeviceList(params) {
  return request({
    url: '/api/v1/groups.json',
    method: 'get',
    params
  })
}

export function fetchGroupDeviceItem(id) {
  return request({
    url: `/api/v1/groups/${id}.json`,
    method: 'get'
  })
}

export function postGroupDevice(data) {
  return request({
    url: '/api/v1/groups',
    method: 'post',
    data
  })
}

export function putGroupDeviceItem(id, data) {
  return request({
    url: `/api/v1/groups/${id}.json`,
    method: 'put',
    data
  })
}

export function deleteGroup(id, data) {
  return request({
    url: `/api/v1/groups/${id}.json`,
    method: 'delete',
    data
  })
}

export function deleteGroupDevice(id, data) {
  return request({
    url: `/api/v1/groups/device/${id}.json`,
    method: 'post',
    data
  })
}
