import { fetchTemplateConfigList, fetchTemplateConfigItem } from './api'

const templateConfig = {
  state: {
    list: [],
    current: {}
  },
  mutations: {
    SET_TEMPLATE_CONFIG_LIST: (state, list) => {
      state.list = list
    },
    SET_TEMPLATE_CONFIG_CURRENT: (state, item) => {
      state.current = item
    }
  },
  actions: {
    async getTemplateConfigList({ commit }, params) {
      const response = await fetchTemplateConfigList(params)
      commit('SET_TEMPLATE_CONFIG_LIST', response.data)
    },
    async getTemplateConfigItem({ commit }, id) {
      const response = await fetchTemplateConfigItem(id)
      commit('SET_TEMPLATE_CONFIG_CURRENT', response.data)
    }
  }
}

const templateConfigGetters = {
  templateConfigList: state => state.templateConfig.list,
  currentTemplateConfig: state => state.templateConfig.current
}

export { templateConfig, templateConfigGetters }
