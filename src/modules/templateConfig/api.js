import request from '@/utils/request'


export function postTemplateConfig(data) {
  return request({
    url: '/api/v1/configs/templates',
    method: 'post',
    data
  })
}

export function fetchTemplateConfigList(params) {
  return request({
    url: '/api/v1/configs/templates.json',
    method: 'get',
    params: params
  })
}

export function fetchTemplateConfigItem(id) {
  return request({
    url: `/api/v1/configs/templates/${id}.json`,
    method: 'get'
  })
}

export function deleteTemplateConfigItem(id) {
  return request({
    url: `/api/v1/configs/templates/${id}.json`,
    method: 'delete'
  })
}

export function updateTemplateConfigItem(id, data) {
  return request({
    url: `/api/v1/configs/templates/${id}.json`,
    method: 'put',
    data
  })
}
