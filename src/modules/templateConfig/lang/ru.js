export default {
  TEMPLATE_CONFIG: {
    PAGE_TITLE: {
      MENU: 'Шаблоны конфигураций',
      LIST: 'Шаблоны конфигураций',
      CREATE: 'Создание шаблона',
      VIEW: 'Просмотр шаблона',
      EDIT: 'Редактирование шаблона'
    },
    ACTIONS: {
      NEW: 'Создать шаблон',
      REMOVE: 'Удаление шаблона'
    }
  }
}
