const _import = require('@/utils/import/_import_' + process.env.NODE_ENV)

export default [
  {
    path: '/template-config',
    component: {
      template: '<router-view></router-view>'
    },
    name: 'template-config',
    meta: {
      title: 'TEMPLATE_CONFIG.PAGE_TITLE.MENU',
      icon: 'table',
      noDropdown: true
    },
    children: [
      {
        path: '',
        component: _import('templateConfig/containers/list'),
        hidden: true,
        name: 'template-config.list'
      },
      {
        path: 'edit/:id',
        component: _import('templateConfig/containers/edit'),
        name: 'template-config.edit',
        hidden: true,
        meta: { title: 'TEMPLATE_CONFIG.PAGE_TITLE.EDIT' }
      },
      {
        path: 'info/:id',
        component: _import('templateConfig/containers/info'),
        name: 'template-config.info',
        hidden: true,
        meta: { title: 'TEMPLATE_CONFIG.PAGE_TITLE.VIEW' }
      },
      {
        path: 'create',
        component: _import('templateConfig/containers/create'),
        name: 'template-config.create',
        hidden: true,
        meta: { title: 'TEMPLATE_CONFIG.PAGE_TITLE.CREATE' }
      }
    ]
  }

]
