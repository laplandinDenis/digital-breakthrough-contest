import request from '@/utils/request'

export function fetchGroupDeviceList() {
  return request({
    url: '/api/v1/groups.json',
    method: 'get'
  })
}

export function postGroupDevice(data) {
  return request({
    url: '/api/v1/groups',
    method: 'post',
    data
  })
}

export function postFile(data) {
	return request({
		url: 'api/v1/files',
		method: 'post',
		data
	})
}
