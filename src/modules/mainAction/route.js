const _import = require('@/utils/import/_import_' + process.env.NODE_ENV)

export default [
  {
    path: 'ftp',
    component: _import('mainAction/containers/ftp'),
    name: 'dashboard.ftp',
    hidden: true,
    meta: {
      title: 'Загрузка файла по ftp. Шаг 1'
    }
  },
  {
    path: 'ftpsecond',
    component: _import('mainAction/containers/ftpsteptwo'),
    hidden: true,
    name: 'dashboard.secondftp',
    meta: {
      title: 'Загрузка файла по ftp. Шаг 2'
    }
  },
  {
    path: 'change-group',
    component: _import('mainAction/containers/changeGroup'),
    name: 'dashboard.change-group',
    hidden: true,
    meta: {
      title: 'DASHBOARD.PAGE_TITLE.CONFIG_GROUP'
    }
  }
]
