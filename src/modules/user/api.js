import request from '@/utils/request'

/**
 *
 * @param params [limit, start, orderby, orderbydesc]
 */
export function fetchUserList(params) {
  return request({
    url: '/api/v1/users.json',
    method: 'get',
    params
  })
}

export function fetchUserItem(id, params) {
  return request({
    url: `/api/v1/users/${id}.json`,
    method: 'get',
    params
  })
}

export function createUser(data) {
  return request({
    url: '/api/v1/users.json',
    method: 'post',
    data
  })
}

export function deleteUser(id) {
  return request({
    url: `/api/v1/users/${id}.json`,
    method: 'delete'
  })
}

export function updateUser(id, data) {
  return request({
    url: `/api/v1/users/${id}.json`,
    method: 'put',
    data
  })
}
