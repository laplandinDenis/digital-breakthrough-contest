const _import = require('@/utils/import/_import_' + process.env.NODE_ENV)

export default {
  path: '/user',
  component: {
    template: '<router-view></router-view>',
    icon: 'user'
  },
  meta: {
    title: 'USER.PAGE_TITLE.MENU',
    noDropdown: true,
    icon: 'peoples'
  },
  children: [
    {
      path: '',
      name: 'user',
      component: _import('user/containers/list'),
      hidden: true,
      meta: { title: 'USER.PAGE_TITLE.LIST' }
    },
    {
      path: 'create',
      component: _import('user/containers/create'),
      name: 'user.create',
      hidden: true,
      meta: { title: 'USER.PAGE_TITLE.CREATE' }
    },
    {
      path: 'edit/:id',
      component: _import('user/containers/edit'),
      name: 'user.edit',
      hidden: true,
      meta: { title: 'USER.PAGE_TITLE.EDIT' }
    }
  ]
}

