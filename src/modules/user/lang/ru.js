/**
 * Created by laplandin on 18.12.17.
 */
export default {
  USER: {
    PAGE_TITLE: {
      MENU: 'Пользователи',
      LIST: 'Список пользователей',
      CREATE: 'Добавление пользователя',
      VIEW: 'Просмотр пользователя',
      EDIT: 'Редактирование пользователя'
    },
    NEW: 'Новый пользователь'
  }
}
