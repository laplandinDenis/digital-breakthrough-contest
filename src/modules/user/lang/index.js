import enLocale from './en'
import ruLocale from './ru'

export default {
  en: enLocale,
  ru: ruLocale
}
