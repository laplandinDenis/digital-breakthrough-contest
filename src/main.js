import Vue from 'vue'
import Element from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import i18n from './lang' // internationalization
import App from './App'
import router from './router'
import store from './store'
import * as filters from './filters' // global filter
import './icons' // icon
import './errorLog'// error log
import './permission' // permissions
import './mock' // All requests for this project use mockjs emulation

Vue.use(Element, {
  i18n: (key, value) => i18n.t(key, value)
})
import 'd3'

// register global utility filters.
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

Vue.config.productionTip = false
window.axios = require('axios')

new Vue({
  el: '#app',
  router,
  store,
  i18n,
  template: '<App/>',
  components: { App }
})
