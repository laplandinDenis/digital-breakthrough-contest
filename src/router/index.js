import Vue from 'vue'
import Router from 'vue-router'
const _import = require('./_import_' + process.env.NODE_ENV)
const _import_modules = require('@/utils/import/_import_' + process.env.NODE_ENV)
// in development-env not use lazy-loading, because lazy-loading too many containers will cause webpack hot update too slow. so only in production use lazy-loading;
// detail: https://panjiachen.github.io/vue-element-admin-site/#/lazy-loading

Vue.use(Router)
// import userRoutes from '@/modules/user/route'
// import templateConfigRoutes from '@/modules/templateConfig/route'
// import typeDeviceRoutes from '@/modules/typeDevice/route'
// import groupDevice from '@/modules/groupDevice/route'
// import device from '@/modules/device/route'
// import configGroup from '@/modules/configGroup/route'
// import mainAction from '@/modules/mainAction/route'
// import queue from '@/modules/queue/route'
import graph from '@/modules/graph/route'
import catalogue from '@/modules/Catalogue/catalogue'
import catalogueItem from '@/modules/Catalogue/catalogue-item'

/**
 * hidden: true                   if `hidden:true` will not show in the sidebar(default is false)
 * redirect: noredirect           if `redirect:noredirect` will no redirct in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    role: ['admin','editor']     will control the page role (you can set multiple roles)
    title: 'title'               the name show in submenu and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar,
    noCache: true                if fasle ,the page will no be cached(default is false)
  }
 **/
export const constantRouterMap = [
  { path: '/login', component: _import('login/index'), hidden: true },
  { path: '/authredirect', component: _import('login/authredirect'), hidden: true },
  { path: '/404', component: _import('errorPage/404'), hidden: true },
  { path: '/401', component: _import('errorPage/401'), hidden: true },
  { path: '/', redirect: 'search' },
  {
    path: '/search',
    component: {
      template: '<router-view></router-view>'
    },
    meta: { title: 'MAIN', icon: 'ic_lupa', noDropdown: true, noCache: true },
    children: [
      {
        path: '',
        component: _import('search/index'),
        name: 'search',
        meta: { title: 'MAIN' }
      }
    ]
  },
  {
    path: '/catalogue',
    component: catalogue,
    meta: { title: 'CATALOGUE', icon: 'table', noDropdown: true, noCache: true },
    children: [],
  },
  {
    path: '/catalogue/:name',
    component: catalogueItem,
  },
  ...graph,
]

export default new Router({
  // mode: 'history', // Back-end support can be opened
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRouterMap
})

export const asyncRouterMap = [
  // ...device,
  // ...groupDevice,
  // ...typeDeviceRoutes,
  // ...templateConfigRoutes,
  // ...configGroup,
  // ...queue,
  // userRoutes
]
