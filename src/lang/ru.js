export default {
  ATTENTION: 'Внимание!',
  REMOVE_CONFIRM: 'Вы действительно хотите удалить данные?',
  CONFIRM: 'Подтвердить',
  CANCEL: 'Отмена',
  CREATE: 'Создать',
  SAVE: 'Сохранить',
  BACK: 'Назад',
  MAIN: 'Главная',
  UPLOAD: 'Загрузить',
  EDIT: 'Редактировать',
  dashboard: 'Главная',
  CATALOGUE: 'КАТАЛОГ'
}
