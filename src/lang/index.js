import Vue from 'vue'
import VueI18n from 'vue-i18n'
import Cookies from 'js-cookie'
import elementEnLocale from 'element-ui/lib/locale/lang/en' // element-ui lang
import elementRuLocale from 'element-ui/lib/locale/lang/ru-RU'// element-ui lang
import enLocale from './en'
import ruLocale from './ru'

import userLocale from '@/modules/user/lang'
import templateConfigLocale from '@/modules/templateConfig/lang'
import typeDeviceLocale from '@/modules/typeDevice/lang'
import deviceGroupLocale from '@/modules/groupDevice/lang'
import deviceLocale from '@/modules/device/lang'
import configGroupLocale from '@/modules/configGroup/lang'
import queueLocale from '@/modules/queue/lang'

// remove caption into pagination input
elementRuLocale.el.pagination.pagesize = '/стр.'
Vue.use(VueI18n)

const messages = {
  en: {
    ...userLocale.en,
    ...enLocale,
    ...elementEnLocale
  },
  ru: {
    ...userLocale.ru,
    ...templateConfigLocale.ru,
    ...typeDeviceLocale.ru,
    ...deviceGroupLocale.ru,
    ...deviceLocale.ru,
    ...configGroupLocale.ru,
    ...ruLocale,
    ...elementRuLocale,
    ...queueLocale.ru
  }
}

const i18n = new VueI18n({
  locale: Cookies.get('language') || 'ru', // set locale
  messages // set locale messages
})

export default i18n
