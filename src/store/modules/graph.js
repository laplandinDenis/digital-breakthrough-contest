import axios from "axios"
import { graphDataMerger } from "@/utils/graph.converter"

const graph = {
  state: {
    graphData: {},
    graphDataCache: []
  },
  mutations: {
    SET_GRAPH_DATA: (state, data) => {
      state.graphData = data
    },
    CLEAR_GRAPH_DATA: (state, data) => {
      state.graphData = {errors: [], results: [{data: []}]}
    },
    PUSH_GRAPH_DATA_CACHE: (state, data) => {
      state.graphDataCache = state.graphDataCache.concat([data])
    },
    POP_GRAPH_DATA_CACHE: (state, count = 1) => {
      state.graphDataCache = state.graphDataCache.slice(0, state.graphDataCache.length - count)
    },
    CLEAR_GRAPH_DATA_CACHE: (state) => {
      state.graphDataCache = []
    }
  },
  actions: {
    async getGraphData({ commit, state }, query){
      const response = await axios.get("/api/graph?q=" + query);
      commit('SET_GRAPH_DATA', response.data.data)
      commit('CLEAR_GRAPH_DATA_CACHE');
      commit('PUSH_GRAPH_DATA_CACHE', { label: query, data: response.data.data });
    },
    async getGraphDataByRelation({ commit, state },{ id, relation, labelName}){
      console.log(id, relation, labelName)
      const response = await axios.get(`/api/graph/${id}/relation?r=${relation}`)
      const merged = graphDataMerger(state.graphData,response.data.data)
      commit('SET_GRAPH_DATA', merged)
      commit('PUSH_GRAPH_DATA_CACHE', {label:id+':' + labelName ,data:merged})
    },
    setGraphData ({commit,state},data){
      commit('SET_GRAPH_DATA', data)
    },
    pushGraphDataCache ({commit,state},data){
      commit('PUSH_GRAPH_DATA_CACHE', data)
    },
    popGraphDataCache({commit,state},count){
      commit('POP_GRAPH_DATA_CACHE', count)
    },
    clearGraphData({commit,state}){
      commit('CLEAR_GRAPH_DATA')
    },
    clearGraphDataCache({commit,state}){
      commit('CLEAR_GRAPH_DATA_CACHE')
    }
  }
}

export default graph
