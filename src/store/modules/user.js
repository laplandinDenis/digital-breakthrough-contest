import store from '@/store'
import { loginByUsername, logout, getUserInfo, refreshToken } from '@/api/login'
import { getToken, setTokens, removeTokens, getRefreshToken } from '@/utils/auth'

const user = {
  state: {
    user: '',
    status: '',
    code: '',
    token: getToken(),
    refreshToken: getRefreshToken(),
    name: '',
    avatar: '',
    introduction: '',
    roles: [],
    setting: {
      articlePlatform: []
    }
  },

  mutations: {
    SET_CODE: (state, code) => {
      state.code = code
    },
    SET_TOKENS: (state, data) => {
      state.token = data.access_token
      state.refreshToken = data.refresh_token
    },
    SET_INTRODUCTION: (state, introduction) => {
      state.introduction = introduction
    },
    SET_SETTING: (state, setting) => {
      state.setting = setting
    },
    SET_STATUS: (state, status) => {
      state.status = status
    },
    SET_NAME: (state, name) => {
      state.name = name
    },
    SET_AVATAR: (state, avatar) => {
      state.avatar = avatar
    },
    SET_ROLES: (state, roles) => {
      state.roles = roles
    }
  },

  actions: {
    // User Login
    LoginByUsername({ commit }, userInfo) {
      const username = userInfo.username.trim()
      return new Promise((resolve, reject) => {
        loginByUsername(username, userInfo.password).then(response => {
          const data = response.data
          commit('SET_TOKENS', data)
          setTokens(response.data)
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },

    // Get user info
    GetUserInfo({ commit, state }) {
      return new Promise((resolve, reject) => {
        getUserInfo().then(response => {
          console.log('getUser', response)
          if (!response.data) { // Since mockjs does not support custom status codes this can only be hack
            reject('error')
          }
          const data = response.data
          commit('SET_ROLES', data.roles)
          commit('SET_NAME', data.username)
          // commit('SET_AVATAR', data.avatar)
          // commit('SET_INTRODUCTION', data.introduction)
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // Login from third side
    // LoginByThirdparty({ commit, state }, code) {
    //   return new Promise((resolve, reject) => {
    //     commit('SET_CODE', code)
    //     loginByThirdparty(state.status, state.email, state.code).then(response => {
    //       commit('SET_TOKENS', response.data.token)
    //       setToken(response.data.token)
    //       resolve()
    //     }).catch(error => {
    //       reject(error)
    //     })
    //   })
    // },

    // Refreshing invalid token

    async RefreshToken({ commit }) {
      const refresh_token = getRefreshToken()
      if (!refresh_token) store.dispatch('FedLogOut')
      try {
        const response = await refreshToken(refresh_token)
        console.log('TOKEN REFRESH SUCCESS', response)
        await commit('SET_TOKENS', response)
        setTokens(response.data)
      } catch (err) {
        throw new Error(err)
      }
    },

    //  Logout
    LogOut({ commit, state }) {
      return new Promise((resolve, reject) => {
        logout().then(() => {
          commit('SET_TOKENS', '')
          commit('SET_ROLES', [])
          removeTokens()
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },

    // Logout
    FedLogOut({ commit }) {
      return new Promise(resolve => {
        commit('SET_TOKENS', '')
        removeTokens()
        resolve()
      })
    },

    // Dynamic permissions change
    ChangeRole({ commit }, role) {
      return new Promise(resolve => {
        commit('SET_TOKENS', role)
        setTokens(role)
        getUserInfo(role).then(response => {
          const data = response.data
          commit('SET_ROLES', data.role)
          commit('SET_NAME', data.name)
          commit('SET_AVATAR', data.avatar)
          commit('SET_INTRODUCTION', data.introduction)
          resolve()
        })
      })
    }
  }
}

export default user
