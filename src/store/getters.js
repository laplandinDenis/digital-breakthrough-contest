import { graphDataConverter } from "@/utils/graph.converter"

const getters = {
  /*
    APP GETTERS
   */
  sidebar: state => state.app.sidebar,
  searchBar: state => state.app.searchBar,
  language: state => state.app.language,
  visitedViews: state => state.app.visitedViews,
  cachedViews: state => state.app.cachedViews,
  /*
   USER GETTERS
   */
  token: state => state.user.token,
  avatar: state => state.user.avatar,
  name: state => state.user.name,
  introduction: state => state.user.introduction,
  status: state => state.user.status,
  roles: state => state.user.roles,
  setting: state => state.user.setting,
  /*
   PERMISSION GETTERS
   */
  permission_routers: state => state.permission.routers,
  addRouters: state => state.permission.addRouters,
  /*
   GRAPH GETTERS
   */
  convertedGraphData: state => graphDataConverter(state.graph.graphData),
  isThereGraphData: state => {
    if(!state.graph.graphData.results) return false
    return !!state.graph.graphData.results[0].data.length
  }
}
export default getters
