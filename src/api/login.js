import request from '@/utils/request'
import authData from '@/api/authConfig'

export function loginByUsername(username, password) {
  const data = Object.assign(authData, { grant_type: 'password', username, password })
  return request({
    url: '/oauth/v2/token',
    method: 'post',
    data
  })
}

export function refreshToken(refresh_token) {
  console.log('TOKEN REFRESHING')
  const data = Object.assign(authData, { grant_type: 'refresh_token', refresh_token })
  return request({
    url: '/oauth/v2/token',
    method: 'post',
    data
  })
}

export function logout() {
  return request({
    url: '/login/logout',
    method: 'post'
  })
}

export function getUserInfo() {
  return request({
    url: '/api/v1/user/current',
    method: 'get'
  })
}

