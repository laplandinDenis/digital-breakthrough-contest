import router from './router'
import store from './store'
import NProgress from 'nprogress' // Progress indicator
import 'nprogress/nprogress.css'// Progress style
import { getToken } from '@/utils/auth'
import { Message } from 'element-ui'

// permissiom judge
function hasPermission(roles, permissionRoles) {
  if (roles.indexOf('admin') >= 0) return true // admin permission pass
  if (!permissionRoles) return true
  return roles.some(role => permissionRoles.indexOf(role) >= 0)
}

const whiteList = ['/login', '/authredirect', '/', '/graph',  '/search', '/catalogue', '/catalogue/energy'] // Not redirect whiteList

router.beforeEach((to, from, next) => {
  NProgress.start() // Open Progress
  if (getToken()) { // is token presented
    if (to.path === '/login') {
      next({ path: '/' })
      NProgress.done() // router in hash Manually change in mode hash redirection will not trigger afterEach temporary hack program ps: history mode no problem, you can delete the line!
    } else {
      if (store.getters.roles.length === 0) { // define is current user has been set user_info
        store.dispatch('GetUserInfo').then(res => { // get user_info
          const roles = res.data.roles
          store.dispatch('GenerateRoutes', { roles }).then(() => { // create available route table
            router.addRoutes(store.getters.addRouters) // Dynamic adding new avalable route tables
            next({ ...to }) // hack for addRoutes completed
          })
        }).catch(err => {
          console.log(`getUser Err ${err}`)
          if (err.response.status === 403) Message.error('Отказано в доступе')
          // store.dispatch('FedLogOut').then(() => {
          //   Message.error('Ошибка авторизации, пожалуйста пройдите авторизацию') // "Verification failed, please log in again"
          //   next({ path: '/login' })
          // })
        })
      } else {
        // we couldn't autochange permissions next() you should remove next permission to determine
        if (hasPermission(store.getters.roles, to.meta.role)) {
          next()//
        } else {
          next({ path: '/401', query: { noGoBack: true }})
          NProgress.done() // router in hash Manually change in mode hash redirection will not trigger afterEach temporary hack program ps: history mode no problem, you can delete the line!
        }
        // Can be deleted ↑
      }
    }
  } else {
    if (whiteList.indexOf(to.path) !== -1) { // In the login whitelist, direct access
      next()
      NProgress.done()
    } else {
      next('/login') // Otherwise all redirect to the login page
      NProgress.done() // Router in the hash mode manually change the hash redirect will not trigger afterEach temporary hack program ps: history mode no problem, you can delete the line!
    }
  }
})


router.afterEach(() => {
  NProgress.done() // End Progress
})
